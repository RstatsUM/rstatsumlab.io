# Contributing to R User Group at the University of Manchester (RUM) Website

Instructions to configure your machine are available at [`README.Rmd`](README.Rmd).

## Add new member

Insert a new entry to [`csv/team.csv`](csv/team.csv).

## Retire a member

Edit [`csv/team.csv`](csv/team.csv) and insert the retire date.

## Add new meeting

Insert a new entry to [`csv/session.csv`](csv/session.csv).

## Add new blog post (GitLab Web IDE)

Visit [GitLab Web IDE](https://gitlab.com/-/ide/project/RstatsUM/rstatsum.gitlab.io/edit/master/-/).

![Screenshot of GitLab Web IDE](img/gitlab-web-ide.png)

Open the file `.gitlab/issue_templates/new-blog-post.md` using the left panel.

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-template.png)

Copy the content of `.gitlab/issue_templates/new-blog-post.md`.

Create a new file inside `content/blog` directory.
Use `YYYY-MM-DD-some-key-words.Rmd` to name your file,
where

- `YYYY` is the year of the blog post
- `MM` is the month of the blog post
- `DD` is the day of the blog post

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-new-file.png)

Paste the content of `.gitlab/issue_templates/new-blog-post.md` inside the new file.
And edit the content.

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-new-file-with-content.png)

After you finished your blog post,
include it in `csv/sessions.csv`.

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-sessions-csv.png)

Now you can commit your changes.

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-commit.png)

Remember to select the option to create a new branch and a merge request.

![Screenshot of GitLab Web IDE](img/gitlab-web-ide-merge-request.png)

Someone will merge your pull request soon.