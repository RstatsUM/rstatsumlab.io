---
title: Meetings
menu: ["main", "footer"]
weight: 3
---

<a href="https://gitlab.com/RstatsUM/website/issues" class="button-brand button-block">Submit a talk</a>

Below is the complete list of upcoming and past meetings.
It is sorted from the most recent one to the oldest one.

```{r echo=FALSE}
source("../../R/rum.R")

# Read information from sessions
calendar_raw <- read.csv(
  "../../csv/sessions.csv",
  header=TRUE,
  stringsAsFactors=FALSE
)
```

```{r echo=FALSE}
calendar <- calendar_raw[order(calendar_raw$date, decreasing = TRUE), ]
formattable(
        calendar,
        list(
          date=future_meetings,
          time=FALSE,
          place=FALSE,
          topic=link_slides,
          announcement=FALSE,
          followup=FALSE,
          slides=FALSE,
          data=FALSE,
          youtube=FALSE
        ),
        row.names=FALSE,
        col.names=c("Date", "Speakers", "Topic"),
        align=c("c", "c", "c"),
        table.attr='class="table"'
)
```