---
date: 2017-08-25
title:  "New Website"
author: "Raniere Silva"
categories: ["announcement"]
tags: ["website"]
---

You probably didn't know that [we have a website](https://rumgroup.github.io/Home/) because, mainly, there was no link to it anywhere.
We didn't advertise our website because we didn't like it
and we where short in hands to help improve it.

We are happy to announce [our new website](https://RstatsUM.gitlab.io/website/).
In future blogs, we will talk more about the techincal choices bewind the new website
and things that we want to improve on it---there are lots of things---and you can help.
For now, we want to say that the website is powered by [https://jekyllrb.com/](Jekyll),
[R Markdown](http://rmarkdown.rstudio.com/) and [GitLab](http://gitlab.com/)
which enable us to edit the content of the website with [RStudio](https://www.rstudio.com/)
and handle processing to GitLab.
