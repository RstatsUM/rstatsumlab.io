---
date: 2019-02-18
title:  "Flexible GIS"
author: "Christian Devenish"
categories:  # Keep one and only one.
- "MMU R User Group"
tags:  # Keep as many as applicable
- "announcement"
- "meeting"
---

Just a quick reminder about our next MMUseR session - this Wednesday, 20th Feb, 12.00 - 13.00 at the MMU Business School (All Saints Park) in BS 3.18. Fraser Baker will be giving a presentation on Flexible GIS in R, with some examples of different GIS processes in R, with emphasis on going beyond GUI based GIS software, eg customising your GIS operations.