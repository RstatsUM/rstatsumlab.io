---
date: 2017-06-23
title:  "R-themed quiz"
author: "Heather Robinson"
categories: []
tags: ["website"]
---

Please save the date for post work drinks and an R-themed quiz at Sandbar at 5.30pm on Thursday 29th of July! This will be a joint event in conjunction with the MMU R user group. We hope to see you all there.
