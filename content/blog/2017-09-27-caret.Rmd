---
date: 2017-09-27
title:  "Using Caret to solve classification problems"
author: "Heather Robinson"
categories: []
tags: ["website"]
---

The next RUM meeting will be held on Monday the 2nd of October in The Congregation, the ground floor meeting room in Vaughan House (Portsmouth Street).

Our topics will be:


*         Using Caret to solve classification problems- Jamie Soul

*         Potential RUM funding stream applications- Reka Solymosi

We are looking for volunteers to contribute talks this term, at any level of R, so don't be shy! We are also grateful for any help with organising events and funding. Please encourage any new starters within your groups to sign up to the RUM listserv and to join the yammer new users group, as we hope to welcome some new faces at the next few meetings.
