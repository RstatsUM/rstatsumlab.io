# R User Group Theme

## Files and Folder

- `assets`

  Contains SASS files to be processed using Hugo Pipes.
- `layouts`

  Contains the HTML parts to be used.
- `static`

  Contains supporting files.

## License

Licensed under the MIT License. See the LICENSE file for more details.

## Credits

* [(Jekyll) Minima theme](https://github.com/jekyll/minima)
