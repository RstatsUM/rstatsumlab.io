---
date: YYYY-MM-DD
title:  "Title goes here"
author: "Your name goes here"
categories:  # Keep one and only one.
- "RUM"
- "MMU R User Group"
- "Manchester R user Group"
tags:  # Keep as many as applicable
- "announcement"
- "meeting"
- "workshop"
- "course"
- "hackthon"
- "opportunity"
- "prize"
- "award"
- "vancacy"
---

![Opening phot description goes here.](../img/photo.jpg)

Content goes here.
